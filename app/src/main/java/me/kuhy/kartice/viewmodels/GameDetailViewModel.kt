package me.kuhy.kartice.viewmodels

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.kuhy.kartice.data.game.GameMetadata
import me.kuhy.kartice.data.repositories.GameRepository
import me.kuhy.kartice.ui.gamedetail.BUNDLE_GAME_NAME
import org.threeten.bp.OffsetDateTime

/**
 * Stores and manages UI-related data for
 * [me.kuhy.kartice.ui.gamedetail.GameDetailActivity].
 * Uses [GameRepository] for data manipulation.
 *
 * @author Ondřej Kuhejda
 */
class GameDetailViewModel(state: SavedStateHandle) : ViewModel() {
    val gameName = state.get<String>(BUNDLE_GAME_NAME)!!
    val game = GameRepository.getGame(gameName)

    /**
     * Updates [GameMetadata.accessTime] of [game] and updates it in [GameRepository].
     */
    fun updateGameLastOpened() {
        viewModelScope.launch {
            GameRepository.setGameAccessTime(gameName, OffsetDateTime.now())
        }
    }

    /**
     * Toggles [GameMetadata.isFavourite] of [game] and updates it in [GameRepository].
     */
    fun toggleIsGameFavourite() {
        viewModelScope.launch {
            GameRepository.toggleIsGameFavourite(gameName)
        }
    }

    /**
     * LiveData boolean that has same value as [GameMetadata.isFavourite].
     */
    fun isGameFavourite() = GameRepository.isGameFavourite(gameName)
}
