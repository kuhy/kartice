package me.kuhy.kartice.viewmodels

import androidx.lifecycle.ViewModel
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.data.repositories.GameRepository

/**
 * Stores and manages UI-related data for
 * [me.kuhy.kartice.ui.gamesearch.GameSearchActivity].
 * Uses [GameRepository] for data manipulation.
 *
 * @author Ondřej Kuhejda
 */
class GameSearchViewModel : ViewModel() {
    /**
     * [List] of all used values for [Game.category].
     */
    fun getGameCategories() = GameRepository.getGameCategories()
}
