package me.kuhy.kartice.viewmodels

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import me.kuhy.kartice.data.game.GameMetadata
import me.kuhy.kartice.data.repositories.GameRepository
import me.kuhy.kartice.ui.settings.SETTINGS_SNACKBAR_DURATION

/**
 * Stores and manages UI-related data for
 * [me.kuhy.kartice.ui.settings.SettingsFragment].
 * Uses [GameRepository] for data manipulation.
 *
 * @author Ondřej Kuhejda
 */
class SettingsViewModel : ViewModel() {
    private val allGames = GameRepository.getGames()

    /**
     * Job that can be cancelled.
     */
    private var job: Job? = null

    /**
     * Set [GameMetadata.isFavourite] to [Boolean.false] in [allGames].
     */
    fun removeAllGamesFromFavourites() {
        job = GlobalScope.launch {
            delay(SETTINGS_SNACKBAR_DURATION.toLong())
            GameRepository.removeAllGamesFromFavourites()
        }
    }

    /**
     * Set [GameMetadata.accessTime] to null in all [allGames].
     */
    fun removeAccessTimeOfAllGames() {
        job = GlobalScope.launch {
            delay(SETTINGS_SNACKBAR_DURATION.toLong())
            GameRepository.removeAccessTimeOfAllGames()
        }

    }

    /**
     * Cancels [job] executed by [removeAccessTimeOfAllGames] or [removeAllGamesFromFavourites].
     */
    fun undoChangeGamesMetadata() {
        job?.cancel()
    }
}
