package me.kuhy.kartice.viewmodels

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.data.repositories.GameRepository
import me.kuhy.kartice.ui.gamelist.ARG_GAME_CARDS
import me.kuhy.kartice.ui.gamelist.ARG_GAME_CATEGORY
import me.kuhy.kartice.ui.gamelist.ARG_GAME_LIST_TYPE
import me.kuhy.kartice.ui.gamelist.ARG_GAME_PLAYERS
import me.kuhy.kartice.ui.gamelist.GameListType

/**
 * Stores and manages UI-related data for
 * [me.kuhy.kartice.ui.gamelist.GameListFragment].
 * Uses [GameRepository] for data manipulation.
 *
 * @author Ondřej Kuhejda
 */
class GameListViewModel(state: SavedStateHandle) : ViewModel() {
    private val games = state.getLiveData<GameListType>(ARG_GAME_LIST_TYPE).switchMap {
        when (it) {
            GameListType.ALL -> GameRepository.getGames()
            GameListType.FAVOURITES -> GameRepository.getFavouriteGames()
            GameListType.SORTED_BY_ACCESS_TIME -> GameRepository.getGamesSortedByAccessTime()
            GameListType.CUSTOM -> {
                Transformations.map(GameRepository.getGames()) { allGames ->
                    allGames.filter { game ->
                        game.isForNumberOfPlayers(state.get<Int>(ARG_GAME_PLAYERS)!!)
                            && (state.get<String>(ARG_GAME_CATEGORY) == null
                            || game.category == state.get<String>(ARG_GAME_CATEGORY))
                            && game.isForNumberOfCards(state.get<Int>(ARG_GAME_CARDS)!!)
                    }
                }
            }
        }
    }

    /**
     * Returns [LiveData] containing [List] of [Game]s.
     * Content of this list is determined by arguments passed to
     * [me.kuhy.kartice.ui.gamelist.GameListFragment].
     */
    fun getFilteredGameList() = games
}
