package me.kuhy.kartice

import android.app.Application
import android.util.Log
import androidx.preference.PreferenceManager
//import com.facebook.stetho.Stetho
import com.jakewharton.threetenabp.AndroidThreeTen
import me.kuhy.kartice.ui.settings.PREF_THEME
import me.kuhy.kartice.utils.ThemePreference
import me.kuhy.kartice.utils.applyTheme

private val TAG = App::class.java.simpleName

/**
 * Custom [Application] for data initialization on startup.
 *
 * Inside init block
 * -----------------
 * Initializes [AndroidThreeTen].
 *
 * @author Ondřej Kuhejda
 */
class App : Application() {
    init {
        Log.d(TAG, "Initializing AndroidThreeTen.")
        AndroidThreeTen.init(this)
    }

    /**
     * Sets the theme of the application based on the [PREF_THEME].
     */
    override fun onCreate() {
        super.onCreate()
        //Stetho.initializeWithDefaults(this)

        applyTheme(ThemePreference.valueOf(PreferenceManager.getDefaultSharedPreferences(this)
            .getString(PREF_THEME, "DEFAULT")!!))
    }
}
