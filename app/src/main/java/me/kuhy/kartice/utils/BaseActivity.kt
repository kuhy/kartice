package me.kuhy.kartice.utils

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import me.kuhy.kartice.R


/**
 * Superclass for most of the activities.
 * It is not suitable for activities that are on the top of the hierarchy,
 * because it displays UP button.
 *
 * Provides helper functions.
 *
 * @author Ondřej Kuhejda
 */
abstract class BaseActivity : AppCompatActivity() {

    override fun setContentView(view: View?, params: ViewGroup.LayoutParams?) {
        super.setContentView(view, params)
        setupToolbar()
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        setupToolbar()
    }

    override fun setContentView(view: View?) {
        super.setContentView(view)
        setupToolbar()
    }

    /**
     * Enables toolbar with UP button.
     */
    private fun setupToolbar() {
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * Helper method that enables toolbar scrolling.
     */
    protected fun enableToolbarScrolling() {
        (findViewById<Toolbar>(R.id.toolbar).layoutParams as AppBarLayout.LayoutParams)
            .scrollFlags = AppBarLayout.LayoutParams
            .SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
    }
}
