package me.kuhy.kartice.utils

import android.os.Build
import androidx.appcompat.app.AppCompatDelegate

/**
 * Enum with possible settings for application theme.
 *
 * @author Ondřej Kuhejda
 */
enum class ThemePreference {LIGHT, DARK, DEFAULT}

/**
 * Sets theme based on the given [themePreference].
 *
 * @author Ondřej Kuhejda
 */
fun applyTheme(themePreference: ThemePreference) {
    when (themePreference) {
        ThemePreference.LIGHT -> {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        ThemePreference.DARK -> {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        ThemePreference.DEFAULT -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY)
            }
        }
    }
}
