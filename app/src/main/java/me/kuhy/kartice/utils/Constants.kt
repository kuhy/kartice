package me.kuhy.kartice.utils

/**
 * Name of the application Room database.
 */
const val DATABASE_NAME = "kartice-db"

/**
 * Folder containing json files with game rules.
 */
const val GAME_RULES_FOLDER = "rules"

/**
 * Folder with CSS files used for displaying of games.
 */
private const val GAME_RULES_STYLES_FOLDER = "styles/"
const val GAME_RULES_STYLES_LIGHT_FILE = GAME_RULES_STYLES_FOLDER + "light.css"
const val GAME_RULES_STYLES_DARK_FILE = GAME_RULES_STYLES_FOLDER + "dark.css"

/**
 * Path to the '.properties' file that stores information about this app.
 */
const val APP_PROPERTIES_FILE = "app.properties"

/**
 * Property in [APP_PROPERTIES_FILE] with version of game rules.
 */
const val APP_PROPERTIES_RULES_VERSION = "rules_version"
