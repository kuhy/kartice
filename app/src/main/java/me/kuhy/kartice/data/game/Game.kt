package me.kuhy.kartice.data.game

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Class that represents card game.
 * Holds data about game such as [name] or [rules].
 *
 * @author Ondřej Kuhejda
 */
@Entity(
    tableName = "game",
    indices = [Index("name")]
)
data class Game(
    @PrimaryKey
    @ColumnInfo(collate = ColumnInfo.LOCALIZED)
    val name: String,

    @ColumnInfo(name = "alternative_names")
    val alternativeNames: String?,

    val description: String?,
    val category: String,

    /**
     * [players] are in the following format:
     * - values are separated with ','
     * - one value can be:
     *     1. number
     *     2. closed interval: two numbers separated with '-'
     *     3. left-bounded interval: number with '+' at the end
     */
    val players: String,

    /**
     * [cards] are in the following format:
     * - values are separated with ','
     * - one value can be:
     *     1. number
     *     2. '?' that indicates that game is for any number of cards
     */
    val cards: String,

    val deck: String?,
    val values: String?,
    val goal: String,

    /**
     * [String] containing rules written in CommonMark with GFM tables support.
     */
    val rules: String
) {
    /**
     * Returns [Boolean.true] if game is for given [number] of players.
     */
    fun isForNumberOfPlayers(number: Int): Boolean {
        players.replace(" ", "").split(",").forEach { s ->
            if (number.isInRange(s) || number.isGreaterOrEqual(s) || number.isEqual(s)) {
                return true
            }
        }
        return false
    }

    private fun Int.isInRange(rangeString: String) = rangeString.contains("-") &&
        rangeString.split("-")[0].toInt().rangeTo(rangeString.split("-")[1].toInt())
            .contains(this)

    private fun Int.isGreaterOrEqual(expression: String) = expression.contains("+") &&
        this >= expression.trimEnd('+').toInt()

    private fun Int.isEqual(numberString: String) = numberString.matches("\\d+".toRegex()) &&
        this == numberString.toInt()

    /**
     * Returns [Boolean.true] if game is for given [number] of cards.
     * [number] must be from the following set: {32, 52, 54, 64, 104, 108}
     */
    fun isForNumberOfCards(number: Int): Boolean {
        cards.replace(" ", "").split(",").forEach { value ->
            if (value == "?") {
                return true
            } else if (number >= value.toInt()) {
                when {
                    value.toInt() % 32 == 0 -> return true
                    value.toInt() % 52 == 0 && number != 64 -> return true
                    value.toInt() % 54 == 0 && number != 64 && number != 104 -> return true
                }
            }
        }
        return false
    }
}
