package me.kuhy.kartice.data

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.data.game.GameDAO
import me.kuhy.kartice.data.game.GameMetadata
import me.kuhy.kartice.data.game.GameMetadataDAO
import me.kuhy.kartice.utils.DATABASE_NAME
import me.kuhy.kartice.utils.APP_PROPERTIES_FILE
import me.kuhy.kartice.utils.APP_PROPERTIES_RULES_VERSION
import me.kuhy.kartice.workers.SeedDatabaseWorker
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter
import splitties.arch.room.roomDb
import splitties.init.appCtx
import java.util.Properties

/**
 * Version of [Game] rules.
 * This information is needed, because database
 * needs to be repopulated when the version changes.
 */
private const val RULES_VERSION = "RULES_VERSION"

/**
 * Singleton for storing data of application.
 * Stores [Game]s and exposes [gameDAO] for manipulation.
 *
 * @author Ondřej Kuhejda
 */
@Database(entities = [Game::class, GameMetadata::class], version = 1, exportSchema = false)
@TypeConverters(AppDatabase.TypeConverters::class)
internal abstract class AppDatabase : RoomDatabase() {

    abstract fun gameDAO(): GameDAO
    abstract fun gameMetadataDao(): GameMetadataDAO

    /**
     * Holds instance of [AppDatabase].
     * Build [AppDatabase] with name [DATABASE_NAME].
     * Add callbacks to run [SeedDatabaseWorker].
     */
    companion object {
        val instance = roomDb<AppDatabase>(DATABASE_NAME) {
            addCallback(object : RoomDatabase.Callback() {
                /**
                 * Populates [AppDatabase] with [Game]s using [SeedDatabaseWorker],
                 * but only when the version of [RULES_VERSION]
                 * is not equal to the version of the actual rules,
                 * which is stored in [APP_PROPERTIES_FILE].
                 */
                override fun onOpen(db: SupportSQLiteDatabase) {
                    super.onOpen(db)

                    val appProperties = Properties()
                    appProperties.load(appCtx.assets.open(APP_PROPERTIES_FILE))
                    val appPropertiesRulesVersion = appProperties.getProperty(APP_PROPERTIES_RULES_VERSION)

                    val sharedPref = appCtx.getSharedPreferences(RULES_VERSION, Context.MODE_PRIVATE)

                    if (sharedPref.getString(RULES_VERSION, "") != appPropertiesRulesVersion) {
                        val request = OneTimeWorkRequestBuilder<SeedDatabaseWorker>().build()
                        WorkManager.getInstance(appCtx).enqueue(request)

                        with(sharedPref.edit()) {
                            putString(RULES_VERSION, appPropertiesRulesVersion)
                            commit()
                        }
                    }
                }
            })
        }
    }

    /**
     * Defines converter for [OffsetDateTime] to [String] and vice versa.
     */
    object TypeConverters {
        private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

        @TypeConverter
        @JvmStatic
        fun toOffsetDateTime(value: String?): OffsetDateTime? {
            return value?.let {
                return formatter.parse(value, OffsetDateTime::from)
            }
        }

        @TypeConverter
        @JvmStatic
        fun fromOffsetDateTime(date: OffsetDateTime?): String? {
            return date?.format(formatter)
        }
    }
}
