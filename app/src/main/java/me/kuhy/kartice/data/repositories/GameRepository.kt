package me.kuhy.kartice.data.repositories

import me.kuhy.kartice.data.AppDatabase
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.data.game.GameMetadata
import org.threeten.bp.OffsetDateTime

/**
 * Singleton that serves as entry point for accessing data related to [Game]s.
 *
 * @author Ondřej Kuhejda
 */
object GameRepository {
    private val gameDAO = AppDatabase.instance.gameDAO()
    private val gameMetadataDAO = AppDatabase.instance.gameMetadataDao()

    /**
     * Returns all [Game]s.
     */
    fun getGames() = gameDAO.getGames()

    /**
     * Returns [Game] with given [name].
     */
    fun getGame(name: String) = gameDAO.getGame(name)

    /**
     * Return all possible values for [Game.category].
     */
    fun getGameCategories() = gameDAO.getGameCategories()

    /**
     * Returns all [Game]s that are in favourites.
     */
    fun getFavouriteGames() = gameDAO.getFavouriteGames()

    /**
     * Returns all already accessed [Game]s sorted by access time.
     */
    fun getGamesSortedByAccessTime() = gameDAO.getGamesSortedByAccessTime()

    /**
     * Updates access time of [Game] with given [name].
     */
    suspend fun setGameAccessTime(name: String, time: OffsetDateTime) = gameMetadataDAO.updateAccessTime(name, time)

    /**
     * Returns true if [Game] with given [name] is in favourites.
     */
    fun isGameFavourite(name: String) = gameMetadataDAO.isFavourite(name)

    /**
     * Toggle value of [GameMetadata.isFavourite] for [Game] with given [name].
     */
    suspend fun toggleIsGameFavourite(name: String) = gameMetadataDAO.toggleIsFavourite(name)

    /**
     * Sets [GameMetadata.isFavourite] to false for all [Game]s.
     */
    suspend fun removeAllGamesFromFavourites() = gameMetadataDAO.removeAllGamesFromFavourites()

    /**
     * Sets [GameMetadata.accessTime] to null for all [Game]s.
     */
    suspend fun removeAccessTimeOfAllGames() = gameMetadataDAO.removeAccessTimeOfAllGames()
}

