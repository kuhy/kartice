package me.kuhy.kartice.data.game

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import org.threeten.bp.OffsetDateTime

/**
 * [Dao] for metadata about games.
 *
 * @author Ondřej Kuhejda
 */
@Dao
interface GameMetadataDAO {
    /**
     * Returns true if [Game] with given [name] is in favourites.
     */
    @Query("SELECT EXISTS(SELECT * FROM game_metadata WHERE game_name = :name AND is_favourite)")
    fun isFavourite(name: String): LiveData<Boolean>

    /**
     * Updates access time of [Game].
     */
    @Query("UPDATE game_metadata SET access_time = :time WHERE game_name = :name")
    suspend fun updateAccessTime(name: String, time: OffsetDateTime)

    /**
     * Toggle value of [GameMetadata.isFavourite] for [Game] with given [name].
     */
    @Query("UPDATE game_metadata SET is_favourite = NOT is_favourite WHERE game_name = :name")
    suspend fun toggleIsFavourite(name: String)

    /**
     * Sets [GameMetadata.isFavourite] to false for all [Game]s.
     */
    @Query("UPDATE game_metadata SET is_favourite = 0")
    suspend fun removeAllGamesFromFavourites()

    /**
     * Sets [GameMetadata.accessTime] to null for all [Game]s.
     */
    @Query("UPDATE game_metadata SET access_time = NULL")
    suspend fun removeAccessTimeOfAllGames()
}
