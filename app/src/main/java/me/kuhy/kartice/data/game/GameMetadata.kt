package me.kuhy.kartice.data.game

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.threeten.bp.OffsetDateTime

/**
 * Holds metadata about game
 * such as [isFavourite] and [accessTime].
 *
 * @author Ondřej Kuhejda
 */
@Entity(
    tableName = "game_metadata",
    foreignKeys = [
        ForeignKey(
            entity = Game::class,
            parentColumns = ["name"],
            childColumns = ["game_name"],
            onDelete = ForeignKey.NO_ACTION
        )
    ]
)
data class GameMetadata (
    /**
     * Name of the game with this metadata.
     */
    @PrimaryKey
    @ColumnInfo(name = "game_name")
    val gameName: String,

    /**
     * If is set to [Boolean.true] then [Game] is in list of favourite [Game]s
     */
    @ColumnInfo(name = "is_favourite")
    var isFavourite: Boolean = false,

    /**
     * Stores time when the [Game] was last accessed in [me.kuhy.kartice.ui.gamedetail.GameDetailActivity].
     */
    @ColumnInfo(name = "access_time")
    var accessTime: OffsetDateTime? = null
)
