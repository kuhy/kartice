package me.kuhy.kartice.data.game

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import me.kuhy.kartice.data.AppDatabase

/**
 * [Dao] for [Game]s.
 * Defines operations for [Game]s above [AppDatabase].
 *
 * @author Ondřej Kuhejda
 */
@Dao
abstract class GameDAO {
    /**
     * Returns all [Game]s sorted by [Game.name].
     */
    @Query("SELECT * FROM game ORDER BY name")
    abstract fun getGames(): LiveData<List<Game>>

    /**
     * Returns [Game] with given name.
     */
    @Query("SELECT * FROM game WHERE name = :gameName")
    abstract fun getGame(gameName: String): LiveData<Game>

    /**
     * Returns all [Game]s with [GameMetadata.isFavourite] equals to true.
     */
    @Query("SELECT * FROM game WHERE EXISTS (SELECT 1 FROM game_metadata WHERE name = game_name AND is_favourite)")
    abstract fun getFavouriteGames(): LiveData<List<Game>>

    /**
     * Returns all already accessed [Game]s sorted by access time.
     */
    @Query("SELECT game.* FROM game JOIN game_metadata ON (name = game_name) WHERE access_time IS NOT NULL ORDER BY access_time DESC")
    abstract fun getGamesSortedByAccessTime(): LiveData<List<Game>>

    /**
     * Returns all encountered values of [Game.category].
     */
    @Query("SELECT DISTINCT category FROM game")
    abstract fun getGameCategories(): LiveData<List<String>>

    /**
     * Insert [Game]s and corresponding [GameMetadata] into the database.
     */
    @Transaction
    open suspend fun insertGames(games: List<Game>) {
        insertOrReplaceGames(games)
        insertOrIgnoreGameMetadata(games.map { GameMetadata(it.name) })
    }

    /**
     * Inserts [Game]s into database.
     * This function replaces [Game]s that are already in database.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract suspend fun insertOrReplaceGames(games: List<Game>)

    /**
     * Inserts [GameMetadata]s into database.
     * This function ignores [GameMetadata]s that are already in database.
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    protected abstract suspend fun insertOrIgnoreGameMetadata(gamesMetadata: List<GameMetadata>)
}
