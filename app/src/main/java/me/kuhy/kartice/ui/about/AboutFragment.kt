package me.kuhy.kartice.ui.about

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import me.kuhy.kartice.R

/**
 * Fragment that sets Preferences from [R.xml.about].
 *
 * @author Ondřej Kuhejda
 */
class AboutFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.about)
    }
}
