package me.kuhy.kartice.ui.gamesearch

import android.os.Bundle
import me.kuhy.kartice.R
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.ui.gamelist.GameListFragment
import me.kuhy.kartice.utils.BaseActivity

/**
 * [BUNDLE_GAME_PLAYERS] is parameter used for filtering list of [Game]s.
 */
const val BUNDLE_GAME_PLAYERS = "BUNDLE_GAME_PLAYERS"

/**
 * [BUNDLE_GAME_CATEGORY] is parameter used for filtering list of [Game]s.
 */
const val BUNDLE_GAME_CATEGORY = "BUNDLE_GAME_CATEGORY"

/**
 * [BUNDLE_GAME_CARDS] is parameter used for filtering list of [Game]s.
 */
const val BUNDLE_GAME_CARDS = "BUNDLE_GAME_CARDS"

/**
 * Shows list of [Game]s inside [GameListFragment].
 *
 * @author Ondřej Kuhejda
 */
class GameResultActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.simple_content)

        val bundle = intent.extras!!
        supportFragmentManager.beginTransaction().replace(R.id.simple_content_container,
                GameListFragment.newInstance(bundle.getInt(BUNDLE_GAME_PLAYERS),
                    bundle.getString(BUNDLE_GAME_CATEGORY),
                    bundle.getInt(BUNDLE_GAME_CARDS))).commit()
    }
}
