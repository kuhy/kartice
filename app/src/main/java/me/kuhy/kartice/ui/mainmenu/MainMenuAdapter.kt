package me.kuhy.kartice.ui.mainmenu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import me.kuhy.kartice.R

/**
 * Adapter for [ListView] in [MainMenuActivity].
 * Takes [headers] and their [descriptions] that will be displayed in [ListView].
 * Each item has also own color from [colors] and icon form [icons].
 *
 * @author Ondřej Kuhejda
 */
class MainMenuAdapter(
    actualContext: Context,
    private val headers: Array<String>,
    private val descriptions: Array<String>,
    private val colors: IntArray,
    private val icons: IntArray
) : ArrayAdapter<String>(actualContext, R.layout.main_menu_item, headers) {

    /**
     * Setups content of [convertView] if it wasn't already setup.
     */
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        if (convertView == null) {
            val inflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val rowView = inflater.inflate(R.layout.main_menu_item, parent, false)
            val firstLine: TextView = rowView.findViewById(R.id.main_menu_item_first_line)
            val secondLine: TextView = rowView.findViewById(R.id.main_menu_item_second_line)
            val icon: ImageView = rowView.findViewById(R.id.main_menu_item_icon)
            val container: ConstraintLayout = rowView.findViewById(R.id.main_menu_item_container)
            firstLine.text = headers[position]
            secondLine.text = descriptions[position]
            icon.setImageResource(icons[position])
            container.setBackgroundColor(colors[position])

            return rowView
        }
        return convertView
    }
}
