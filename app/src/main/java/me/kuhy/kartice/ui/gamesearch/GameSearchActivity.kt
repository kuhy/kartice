package me.kuhy.kartice.ui.gamesearch

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import androidx.activity.viewModels
import com.shawnlin.numberpicker.NumberPicker
import me.kuhy.kartice.R
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.databinding.GameSearchBinding
import me.kuhy.kartice.utils.BaseActivity
import me.kuhy.kartice.viewmodels.GameSearchViewModel

private const val MAX_NUMBER_OF_PLAYERS = 9
private const val DEFAULT_NUMBER_OF_PLAYERS = 4

/**
 * Activity that provides functionality for filtering [Game]s.
 * It uses [NumberPicker]s for selecting desired values for each category:
 * - [Game.players]
 * - [Game.cards]
 * - [Game.cards]
 *
 * @author Ondřej Kuhejda
 */
class GameSearchActivity : BaseActivity() {
    private lateinit var binding: GameSearchBinding

    private val vm: GameSearchViewModel by viewModels()

    /**
     * Sets [R.layout.game_search] as layout. (it contains 3 [NumberPicker]s)
     * Fills [NumberPicker]s with values such as game categories retrieved from [GameSearchViewModel].
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = GameSearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.gameSearchButton.setOnClickListener { search() }

        for (picker in listOf(binding.gameSearchPickerPlayers, binding.gameSearchPickerCategory,
            binding.gameSearchPickerCards)) {
            picker.setSelectedTypeface(Typeface.create(picker.typeface, Typeface.BOLD))
            picker.minValue = 1
        }

        binding.gameSearchPickerPlayers.maxValue = MAX_NUMBER_OF_PLAYERS
        binding.gameSearchPickerPlayers.value = DEFAULT_NUMBER_OF_PLAYERS

        vm.getGameCategories().observe(this) { categories ->
            val categoriesList = ArrayList(categories)
            categoriesList.add(0, "*")
            binding.gameSearchPickerCategory.maxValue = categoriesList.size
            binding.gameSearchPickerCategory.displayedValues = categoriesList.map { it.toString() }.toTypedArray()
            binding.gameSearchPickerCategory.value = 1
        }

        val cardsList = resources.getIntArray(R.array.search_cards)
        binding.gameSearchPickerCards.maxValue = cardsList.size
        binding.gameSearchPickerCards.displayedValues = cardsList.map { it.toString() }.toTypedArray()
        binding.gameSearchPickerCards.value = 3
    }

    /**
     * This method is called when user clicks on [R.id.game_search_button].
     * It starts new [GameResultActivity] and sends data from [NumberPicker]s to it.
     */
    private fun search() {
        val intent = Intent(this, GameResultActivity::class.java)

        intent.putExtra(BUNDLE_GAME_PLAYERS, binding.gameSearchPickerPlayers.value)
        intent.putExtra(BUNDLE_GAME_CARDS,
            binding.gameSearchPickerCards.displayedValues[binding.gameSearchPickerCards.value - 1].toInt())
        if (binding.gameSearchPickerCategory.value != 1) {
            intent.putExtra(BUNDLE_GAME_CATEGORY,
                binding.gameSearchPickerCategory.displayedValues[binding.gameSearchPickerCategory.value - 1])
        }

        startActivity(intent)
    }
}
