package me.kuhy.kartice.ui.settings

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.snackbar.Snackbar
import me.kuhy.kartice.R
import me.kuhy.kartice.utils.ThemePreference
import me.kuhy.kartice.utils.applyTheme
import me.kuhy.kartice.viewmodels.SettingsViewModel

/**
 * Duration of [Snackbar]s used in settings (in milliseconds).
 */
const val SETTINGS_SNACKBAR_DURATION = 5000

/**
 * [PreferenceFragmentCompat] that sets [R.xml.settings] as layout.
 * Enables user to change settings of application.
 *
 * @author Ondřej Kuhejda
 */
class SettingsFragment : PreferenceFragmentCompat() {

    private val vm: SettingsViewModel by viewModels()

    /**
     * Sets [R.xml.settings] as layout.
     * Also sets listeners for some [Preference]s.
     */
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings)

        findPreference<Preference>(PREF_DELETE_HISTORY)!!.setOnPreferenceClickListener {
            Snackbar.make(requireView(), R.string.pref_data_delete_history_action, SETTINGS_SNACKBAR_DURATION)
                .setAction(R.string.undo) { vm.undoChangeGamesMetadata() }
                .show()
            vm.removeAccessTimeOfAllGames()
            true
        }

        findPreference<Preference>(PREF_DELETE_FAVOURITES)!!.setOnPreferenceClickListener {
            Snackbar.make(requireView(), R.string.pref_data_delete_favourite_action, SETTINGS_SNACKBAR_DURATION)
                .setAction(R.string.undo) { vm.undoChangeGamesMetadata() }
                .show()
            vm.removeAllGamesFromFavourites()
            true
        }

        findPreference<ListPreference>(PREF_THEME)!!.setOnPreferenceChangeListener { _, newValue ->
            applyTheme(ThemePreference.valueOf(newValue as String))
            true
        }
    }
}
