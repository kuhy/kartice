package me.kuhy.kartice.ui.gamedetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.kuhy.kartice.R
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.databinding.GameDetailItemBinding
import splitties.init.appCtx

/**
 * Adapter for [Game] fields.
 *
 * @author Ondřej Kuhejda
 */
class GameDetailAdapter : RecyclerView.Adapter<GameDetailAdapter.GameDetailViewHolder>() {
    private var gameParameters = emptyList<Pair<String, String>>()

    /** Selected [game] non-null fields and corresponding
     * [String] resources are added into the [gameParameters].
     */
    fun setData(game: Game) {
        gameParameters = listOf(R.string.game_alternative_names to game.alternativeNames,
            R.string.game_description to game.description, R.string.game_category to game.category,
            R.string.game_players to game.players, R.string.game_cards to game.cards, R.string.game_deck to game.deck,
            R.string.game_values to game.values, R.string.game_goal to game.goal)
            .filter { it.second != null }.map { Pair(appCtx.getString(it.first) + ":", it.second!!) }
        notifyDataSetChanged()
    }

    /**
     * [RecyclerView.ViewHolder] for [GameDetailItemBinding].
     */
    class GameDetailViewHolder(private val binding: GameDetailItemBinding) : RecyclerView.ViewHolder(binding.root) {
       fun bind(key: String, value: String) {
           binding.gameDetailItemKey.text = key
           binding.gameDetailItemValue.text = value
       }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameDetailViewHolder {
        return GameDetailViewHolder(GameDetailItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return gameParameters.size
    }

    override fun onBindViewHolder(holder: GameDetailViewHolder, position: Int) {
        holder.bind(gameParameters[position].first, gameParameters[position].second)
    }
}
