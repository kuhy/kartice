package me.kuhy.kartice.ui.gamedetail

import android.os.Bundle
import android.text.style.AbsoluteSizeSpan
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import io.noties.markwon.AbstractMarkwonPlugin
import io.noties.markwon.BlockHandlerDef
import io.noties.markwon.Markwon
import io.noties.markwon.MarkwonVisitor
import io.noties.markwon.core.MarkwonTheme
import io.noties.markwon.ext.tables.TablePlugin
import me.kuhy.kartice.R
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.databinding.GameDetailBinding
import me.kuhy.kartice.utils.BaseActivity
import me.kuhy.kartice.viewmodels.GameDetailViewModel
import org.commonmark.node.Heading
import org.commonmark.node.Node
import splitties.init.appCtx

/**
 * Name of displayed [Game].
 */
const val BUNDLE_GAME_NAME = "BUNDLE_GAME_NAME"

private val TAG = GameDetailActivity::class.java.simpleName

/**
 * [BaseActivity] that shows rules of one [Game] in [androidx.core.widget.NestedScrollView].
 *
 * @author Ondřej Kuhejda
 */
class GameDetailActivity : BaseActivity() {
    private val vm: GameDetailViewModel by viewModels()
    private var isGameFavouriteChanged = false

    private lateinit var binding: GameDetailBinding
    private lateinit var markwon: Markwon

    /**
     * Sets layout to [R.layout.game_detail] and enables [R.id.toolbar] scrolling via [enableToolbarScrolling].
     * Displays [Game] by calling [displayGame].
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initMarkwon()

        binding = GameDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        enableToolbarScrolling()

        supportActionBar?.title = vm.gameName

        vm.updateGameLastOpened()

        displayGame()
    }

    /**
     * Initializes [markwon] that is responsible for displaying [Game] rules.
     */
    private fun initMarkwon() {
        val spacingBeforeHeader = (10 * appCtx.resources.displayMetrics.density + .5f).toInt()
        val spacingAfterHeader = (5 * appCtx.resources.displayMetrics.density + .5f).toInt()
        markwon = Markwon.builder(this)
            .usePlugin(TablePlugin.create(this))
            .usePlugin(object: AbstractMarkwonPlugin() {
                override fun configureTheme(builder: MarkwonTheme.Builder) {
                    builder.apply {
                        headingBreakHeight(0)
                    }
                }

                override fun configureVisitor(builder: MarkwonVisitor.Builder) {
                    builder.blockHandler(object : BlockHandlerDef() {
                        override fun blockStart(visitor: MarkwonVisitor, node: Node) {
                            if (node is Heading) {
                                val start = visitor.length()
                                visitor.ensureNewLine()
                                visitor.forceNewLine()
                                visitor.setSpans(start, AbsoluteSizeSpan(spacingBeforeHeader))
                            } else {
                                super.blockStart(visitor, node)
                            }
                        }

                        override fun blockEnd(visitor: MarkwonVisitor, node: Node) {
                            if (node is Heading) {
                                val start = visitor.length()
                                visitor.ensureNewLine()
                                visitor.forceNewLine()
                                visitor.setSpans(start, AbsoluteSizeSpan(spacingAfterHeader))
                            } else {
                                super.blockEnd(visitor, node)
                            }
                        }
                    })
                }
            })
            .build()
    }

    /**
     * Shows info about [Game] inside [R.id.game_detail_rules].
     */
    private fun displayGame() {
        binding.gameDetailRules.isHapticFeedbackEnabled = false
        binding.gameDetailRules.setOnLongClickListener { true }
        binding.gameDetailRules.isLongClickable = false

        binding.gameDetailRecycler.apply {
            layoutManager = object : LinearLayoutManager(context) {
                override fun canScrollVertically() = false
            }
            adapter = GameDetailAdapter()
        }

        vm.game.observe(this, Observer { game ->
            Log.d(TAG, game.rules)
            markwon.setMarkdown(binding.gameDetailRules, game.rules)
            (binding.gameDetailRecycler.adapter as GameDetailAdapter).setData(game)
        })
    }

    /**
     * Creates [menu] from [R.menu.simple_menu] and sets [R.id.simple_menu_button] icon
     * depending on value of [GameDetailViewModel.isGameFavourite].
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.simple_menu, menu)
        vm.isGameFavourite().observe(this, Observer { isFavourite ->
            menu.findItem(R.id.simple_menu_button)!!.icon = ContextCompat.getDrawable(this,
                if (isFavourite) R.drawable.ic_favorite_white_24dp else R.drawable.ic_favorite_border_white_24dp)
            if (isGameFavouriteChanged) {
                Snackbar.make(binding.gameDetailScrollView, String.format(resources
                    .getString(if (isFavourite) R.string.game_add_to_favourites else R.string.game_remove_from_favourites),
                    vm.gameName), LENGTH_SHORT).show()
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Calls [GameDetailViewModel.toggleIsGameFavourite] when [R.id.simple_menu_button] is clicked.
     * Shows [toast] with information about change.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.simple_menu_button -> {
                isGameFavouriteChanged = true
                vm.toggleIsGameFavourite()
                true
            }
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
