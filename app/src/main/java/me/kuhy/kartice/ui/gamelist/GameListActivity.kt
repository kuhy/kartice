package me.kuhy.kartice.ui.gamelist

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.tabs.TabLayout
import me.kuhy.kartice.R
import me.kuhy.kartice.databinding.GameListTabsBinding
import me.kuhy.kartice.utils.BaseActivity
import me.kuhy.kartice.viewmodels.GameListViewModel

/**
 * Activity that shows inside [R.layout.game_list_tabs] (in [R.id.game_list_container]) game lists in three tabs
 * for [GameListViewModel.getFilteredGameList].
 *
 * @author Ondřej Kuhejda
 */
class GameListActivity : BaseActivity() {

    /**
     * Sets content to [R.layout.game_list_tabs] and calls [enableToolbarScrolling].
     * Creates [GameListSectionsPagerAdapter] for [R.id.game_list_container].
     * Sets second tab as default.
     */
    private lateinit var gameListSectionsPagerAdapter: GameListSectionsPagerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = GameListTabsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        enableToolbarScrolling()

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        gameListSectionsPagerAdapter = GameListSectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        binding.gameListContainer.adapter = gameListSectionsPagerAdapter

        binding.gameListContainer.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.gameListTabs))
        binding.gameListTabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(binding.gameListContainer))
        binding.gameListTabs.getTabAt(1)?.select()
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to one of the tabs.
     */
    inner class GameListSectionsPagerAdapter(fm: FragmentManager) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return when (position) {
                0 -> GameListFragment.newInstance(GameListType.FAVOURITES)
                1 -> GameListFragment.newInstance(GameListType.ALL)
                2 -> GameListFragment.newInstance(GameListType.SORTED_BY_ACCESS_TIME)
                else -> throw IllegalArgumentException()
            }
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 3
        }
    }
}
