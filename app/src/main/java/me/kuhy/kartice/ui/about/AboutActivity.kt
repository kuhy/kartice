package me.kuhy.kartice.ui.about

import android.os.Bundle
import me.kuhy.kartice.R
import me.kuhy.kartice.utils.BaseActivity

/**
 * [BaseActivity] that shows info about application.
 *
 * @author Ondřej Kuhejda
 */
class AboutActivity : BaseActivity() {

    /**
     * It sets [AboutFragment] as content.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.simple_content)

        supportFragmentManager.beginTransaction()
                .replace(R.id.simple_content_container, AboutFragment())
                .commit()
    }
}
