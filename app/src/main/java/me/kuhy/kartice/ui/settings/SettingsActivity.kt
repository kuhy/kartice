package me.kuhy.kartice.ui.settings

import android.os.Bundle
import me.kuhy.kartice.R
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.ui.gamelist.GameListFragment
import me.kuhy.kartice.utils.BaseActivity

/**
 * If set, shows [Game.alternativeNames] in [GameListFragment].
 */
const val PREF_SHOW_ALT_NAMES = "pref_show_alternative_names"

/**
 * Theme of the application.
 */
const val PREF_THEME = "pref_theme"

/**
 * Deletes whole history of viewing game.
 */
const val PREF_DELETE_HISTORY = "pref_delete_history"

/**
 * Removes all games from favourites.
 */
const val PREF_DELETE_FAVOURITES = "pref_delete_favourites"

/**
 * Activity that represents settings.
 * Sets [SettingsFragment] as an content.
 *
 * @author Ondřej Kuhejda
 */
class SettingsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.simple_content)

        supportFragmentManager.beginTransaction()
                .replace(R.id.simple_content_container, SettingsFragment())
                .commit()
    }
}
