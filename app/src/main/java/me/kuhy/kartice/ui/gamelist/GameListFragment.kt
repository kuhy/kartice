package me.kuhy.kartice.ui.gamelist

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import me.kuhy.kartice.R
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.databinding.GameListBinding
import me.kuhy.kartice.ui.settings.PREF_SHOW_ALT_NAMES
import me.kuhy.kartice.viewmodels.GameListViewModel
import java.lang.IllegalArgumentException

/**
 * Key for passing [GameListType] values into new [GameListFragment]s.
 * This value determines which list of games will be displayed.
 */
const val ARG_GAME_LIST_TYPE = "ARG_GAME_LIST_TYPE"

/**
 * Types of lists displayed in [GameListFragment].
 */
enum class GameListType { ALL, SORTED_BY_ACCESS_TIME, FAVOURITES, CUSTOM }

/**
 * [ARG_GAME_PLAYERS] is parameter used for filtering [GameListType.CUSTOM] list of [Game]s.
 */
const val ARG_GAME_PLAYERS = "ARG_GAME_PLAYERS"

/**
 * [ARG_GAME_CATEGORY] is parameter used for filtering [GameListType.CUSTOM] list of [Game]s.
 */
const val ARG_GAME_CATEGORY = "ARG_GAME_CATEGORY"

/**
 * [ARG_GAME_CARDS] is parameter used for filtering [GameListType.CUSTOM] list of [Game]s.
 */
const val ARG_GAME_CARDS = "ARG_GAME_CARDS"

private val TAG = GameListFragment::class.java.simpleName

/**
 * A placeholder [Fragment] containing a list of games.
 * Uses [R.layout.game_list] that contains [androidx.recyclerview.widget.RecyclerView].
 *
 * @author Ondřej Kuhejda
 */
class GameListFragment : Fragment() {
    private var _binding: GameListBinding? = null
    private val binding get() = _binding!!

    private val vm: GameListViewModel by viewModels()

    private val observer = Observer<List<Game>> {
        it?.apply {
            (binding.gameListRecycler.adapter as GameListAdapter).games = it
            if (it.isEmpty()) {
                binding.gameListRecycler.visibility = View.GONE
                binding.gameListEmpty.visibility = View.VISIBLE
            } else {
                binding.gameListRecycler.visibility = View.VISIBLE
                binding.gameListEmpty.visibility = View.GONE
            }
        }
        Log.d(TAG, "games LiveData in Fragment changed")
    }

    /**
     * Sets layout to [R.layout.game_list].
     * Calls [androidx.recyclerview.widget.RecyclerView.setAdapter] on new [GameListAdapter].
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = GameListBinding.inflate(inflater, container, false)
        binding.gameListRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = GameListAdapter(PreferenceManager.getDefaultSharedPreferences(context)
                    .getBoolean(PREF_SHOW_ALT_NAMES, false))
        }
        return binding.root
    }

    /**
     * Observes [LiveData] obtained from [GameListViewModel.getFilteredGameList] via [observer].
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        vm.getFilteredGameList().observe(viewLifecycleOwner, observer)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Singleton for creating new instances.
     */
    companion object {

        /**
         * Takes [GameListType] of [Game]s that will be displayed in [androidx.recyclerview.widget.RecyclerView].
         */
        fun newInstance(type: GameListType): GameListFragment {
            if (type == GameListType.CUSTOM) {
                throw IllegalArgumentException("This method cannot by used with custom $ARG_GAME_LIST_TYPE.")
            }
            val fragment = GameListFragment()
            val args = Bundle()
            Log.d(TAG, "creating new instance with $ARG_GAME_LIST_TYPE: $type")
            args.putSerializable(ARG_GAME_LIST_TYPE, type)
            fragment.arguments = args
            return fragment
        }

        /**
         * Creates new [GameListFragment] with [GameListType.CUSTOM] type of list of [Game]s.
         * [Game]s are filtered using [numberOfPlayers], [gameCategory] and [numberOfCards].
         */
        fun newInstance(numberOfPlayers: Int, gameCategory: String?, numberOfCards: Int): GameListFragment {
            val fragment = GameListFragment()
            val args = Bundle()
            Log.d(TAG, "creating new ${GameListType.CUSTOM} instance")
            args.putSerializable(ARG_GAME_LIST_TYPE, GameListType.CUSTOM)
            args.putInt(ARG_GAME_PLAYERS, numberOfPlayers)
            gameCategory?.let { args.putString(ARG_GAME_CATEGORY,gameCategory) }
            args.putInt(ARG_GAME_CARDS, numberOfCards)
            fragment.arguments = args
            return fragment
        }
    }
}
