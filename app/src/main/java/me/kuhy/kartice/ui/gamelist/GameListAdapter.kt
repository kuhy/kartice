package me.kuhy.kartice.ui.gamelist

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import me.kuhy.kartice.R
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.ui.gamedetail.BUNDLE_GAME_NAME
import me.kuhy.kartice.ui.gamedetail.GameDetailActivity
import me.kuhy.kartice.ui.gamelist.GameListAdapter.ViewHolder1
import me.kuhy.kartice.ui.gamelist.GameListAdapter.ViewHolder2

/**
 * [RecyclerView.Adapter] that have two modes depending on [showAlternativeNames].
 * If [showAlternativeNames] is set to [Boolean.true] then is used [ViewHolder2] for [Game]s with
 * [Game.alternativeNames] else is used [ViewHolder1].
 *
 * @author Ondřej Kuhejda
 */
class GameListAdapter(private val showAlternativeNames: Boolean) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**
     * [List] of [Game]s that will be displayed inside [RecyclerView].
     */
    var games: List<Game> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    /**
     * Abstract class that defines default [TextView]s for [Game] displaying.
     */
    abstract class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        /**
         * [TextView] for [Game.name].
         */
        abstract val name: TextView
        /**
         * [TextView] for [Game.players].
         */
        val players: TextView = view.findViewById(R.id.game_list_item_players)
        /**
         * [TextView] for [Game.category].
         */
        val category: TextView = view.findViewById(R.id.game_list_item_category)
    }

    /**
     * [ViewHolder] for displaying [Game] without [Game.alternativeNames].
     */
    class ViewHolder1(view: View) : ViewHolder(view) {
        override val name: TextView = view.findViewById(R.id.game_list_item_1_name)
    }

    /**
     * [ViewHolder] for displaying [Game] with [Game.alternativeNames].
     */
    class ViewHolder2(view: View) : ViewHolder(view) {
        override val name: TextView = view.findViewById(R.id.game_list_item_2_name)
        /**
         * [TextView] for [Game.alternativeNames].
         */
        val alternativeNames: TextView = view.findViewById(R.id.game_list_item_2_alternative_names)
    }

    /**
     * Returns [ViewType.ordinal].
     */
    override fun getItemViewType(position: Int): Int {
        return when (games[position].alternativeNames == null || !showAlternativeNames) {
            true -> ViewType.NORMAL.ordinal
            false -> ViewType.WITH_ALTERNATIVE_NAMES.ordinal
        }
    }

    /**
     * Creates new [View] from [R.layout.game_list_item_1] for [Game]s with [ViewType.NORMAL].
     * For [ViewType.WITH_ALTERNATIVE_NAMES] creates new [View] from [R.layout.game_list_item_2].
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (ViewType.values()[viewType]) {
            ViewType.NORMAL -> ViewHolder1(LayoutInflater.from(parent.context)
                    .inflate(R.layout.game_list_item_1, parent, false))
            ViewType.WITH_ALTERNATIVE_NAMES -> ViewHolder2(LayoutInflater.from(parent.context)
                    .inflate(R.layout.game_list_item_2, parent, false))
        }
    }

    /**
     * Replaces the content of [holder] with data from [Game] with given [position] inside [games].
     * For [ViewHolder2] replaces also [ViewHolder2.alternativeNames].
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.name.text = games[position].name
            holder.players.text = games[position].players
            holder.category.text = games[position].category
            holder.view.setOnClickListener { view ->
                val intent = Intent(view.context, GameDetailActivity::class.java)
                intent.putExtra(BUNDLE_GAME_NAME, games[position].name)
                view.context.startActivity(intent)
            }
        }
        if (holder is ViewHolder2) {
            holder.alternativeNames.text = games[position].alternativeNames
        }
    }

    override fun getItemCount() = games.size

    /**
     * [ViewType] is used by [getItemViewType].
     * Represents type of a [Game] and corresponding [ViewHolder].
     */
    private enum class ViewType { NORMAL, WITH_ALTERNATIVE_NAMES }
}
