package me.kuhy.kartice.ui.mainmenu

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import me.kuhy.kartice.R
import me.kuhy.kartice.databinding.MainMenuBinding
import me.kuhy.kartice.ui.about.AboutActivity
import me.kuhy.kartice.ui.gamelist.GameListActivity
import me.kuhy.kartice.ui.gamesearch.GameSearchActivity
import me.kuhy.kartice.ui.settings.SettingsActivity
import me.kuhy.kartice.utils.BaseActivity

/**
 * [MainMenuActivity] is called on startup.
 * Shows main menu inside [ListView].
 * Only [Activity] that isn't derived from [me.kuhy.kartice.utils.BaseActivity]
 * because is on top of hierarchy.
 *
 * @author Ondřej Kuhejda
 */
class MainMenuActivity : AppCompatActivity() {

    /**
     * Sets content to [R.layout.main_menu] with [BaseActivity.setupToolbar].
     * Sets [MainMenuAdapter] as adapter for [R.id.main_menu_list_view].
     * Sets [AdapterView.OnItemClickListener] for [R.id.main_menu_list_view]
     * that starts new [BaseActivity] depending on item that was clicked.
     */
    @Suppress("MagicNumber")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = MainMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(findViewById(R.id.toolbar))

        val values = resources.getStringArray(R.array.main_menu_list_values)
        val descriptions = resources.getStringArray(R.array.main_menu_list_descriptions)
        val colors = resources.getIntArray(R.array.main_menu_list_colors)
        val resIcons = resources.obtainTypedArray(R.array.main_menu_list_icons)
        val icons = IntArray(resIcons.length()) { i -> resIcons.getResourceId(i, -1) }
        resIcons.recycle()

        binding.mainMenuListView.adapter = MainMenuAdapter(this, values, descriptions, colors, icons)
        binding.mainMenuListView.setOnItemClickListener { _: AdapterView<*>, _: View, i: Int, _: Long ->
            when (i) {
                0 -> startActivity(Intent(this, GameListActivity::class.java))
                1 -> startActivity(Intent(this, GameSearchActivity::class.java))
                2 -> startActivity(Intent(this, SettingsActivity::class.java))
                3 -> startActivity(Intent(this, AboutActivity::class.java))
            }
        }
    }
}
