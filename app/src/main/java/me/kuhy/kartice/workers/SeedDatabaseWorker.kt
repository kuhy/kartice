package me.kuhy.kartice.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.coroutineScope
import me.kuhy.kartice.data.AppDatabase
import me.kuhy.kartice.data.game.Game
import me.kuhy.kartice.utils.GAME_RULES_FOLDER
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import java.io.IOException

private val TAG = SeedDatabaseWorker::class.java.simpleName

/**
 * Parses [Game]s from YAML files and then puts them into the database.
 *
 * @author Ondřej Kuhejda
 */
class SeedDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {
    override suspend fun doWork(): Result = coroutineScope {
        try {
            val yaml = Yaml(Constructor(GameYaml::class.java))
            val parsedGames = ArrayList<Game>()
            applicationContext.assets.list(GAME_RULES_FOLDER)!!.forEach { subfolder ->
                applicationContext.assets.list("$GAME_RULES_FOLDER/$subfolder")!!.forEach { file ->
                    applicationContext.assets.open("$GAME_RULES_FOLDER/$subfolder/$file").use { input ->
                        Log.d(TAG, "Parsing game: $file")
                        val gameYaml: GameYaml = yaml.load(input)
                        parsedGames.add(Game(gameYaml.name!!, gameYaml.alternativeNames, gameYaml.description,
                            gameYaml.category!!, gameYaml.players!!, gameYaml.cards!!, gameYaml.deck, gameYaml.values,
                            gameYaml.goal!!, gameYaml.rules!!))
                    }
                }
            }
            AppDatabase.instance.gameDAO().insertGames(parsedGames)
            Log.i(TAG, "${parsedGames.size} games inserted into DB")
            Result.success()
        } catch (ioException: IOException) {
            Log.e(TAG, "Error seeding database", ioException)
            Result.failure()
        }
    }

    /**
     * Class that represents game parsed from the YAML file.
     * This is needed because of the nature of [org.yaml.snakeyaml] used for parsing.
     */
    class GameYaml(
        var name: String? = null,
        var alternativeNames: String? = null,
        var description: String? = null,
        var category: String? = null,
        var players: String? = null,
        var cards: String? = null,
        var deck: String? = null,
        var values: String? = null,
        var goal: String? = null,
        var rules: String? = null
    )
}

