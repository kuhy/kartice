package me.kuhy.kartice.data.game

import org.junit.Assert
import org.junit.Test

class GameTest {
    @Test
    fun isForNumberOfPlayers_number() {
        Assert.assertTrue(gameForPlayers("1").isForNumberOfPlayers(1))
        Assert.assertTrue(gameForPlayers("10").isForNumberOfPlayers(10))
        Assert.assertTrue(gameForPlayers("4, 8, 16").isForNumberOfPlayers(8))

        Assert.assertFalse(gameForPlayers("1").isForNumberOfPlayers(3))
        Assert.assertFalse(gameForPlayers("4, 8, 16").isForNumberOfPlayers(10))
    }

    @Test
    fun isForNumberOfPlayers_range() {
        Assert.assertTrue(gameForPlayers("1-3").isForNumberOfPlayers(1))
        Assert.assertTrue(gameForPlayers("10-13").isForNumberOfPlayers(11))
        Assert.assertTrue(gameForPlayers("1, 3-5, 8").isForNumberOfPlayers(3))

        Assert.assertFalse(gameForPlayers("1-3").isForNumberOfPlayers(5))
        Assert.assertFalse(gameForPlayers("1, 3-5, 8").isForNumberOfPlayers(2))
    }

    @Test
    fun isForNumberOfPlayers_over() {
        Assert.assertTrue(gameForPlayers("1-3, 8+").isForNumberOfPlayers(8))
        Assert.assertTrue(gameForPlayers("5, 8+").isForNumberOfPlayers(11))

        Assert.assertFalse(gameForPlayers("1-3, 8+").isForNumberOfPlayers(5))
        Assert.assertFalse(gameForPlayers("1, 3-5, 8+").isForNumberOfPlayers(2))
    }

    @Test
    fun isForNumberOfCards_32() {
        Assert.assertTrue(gameForCards("32").isForNumberOfCards(32))
        Assert.assertTrue(gameForCards("32").isForNumberOfCards(52))
        Assert.assertTrue(gameForCards("32").isForNumberOfCards(54))
        Assert.assertTrue(gameForCards("32").isForNumberOfCards(64))
        Assert.assertTrue(gameForCards("32").isForNumberOfCards(104))
        Assert.assertTrue(gameForCards("32").isForNumberOfCards(108))
    }

    @Test
    fun isForNumberOfCards_52() {
        Assert.assertFalse(gameForCards("52").isForNumberOfCards(32))
        Assert.assertTrue(gameForCards("52").isForNumberOfCards(52))
        Assert.assertTrue(gameForCards("52").isForNumberOfCards(54))
        Assert.assertFalse(gameForCards("52").isForNumberOfCards(64))
        Assert.assertTrue(gameForCards("52").isForNumberOfCards(104))
        Assert.assertTrue(gameForCards("52").isForNumberOfCards(108))
    }

    @Test
    fun isForNumberOfCards_54() {
        Assert.assertFalse(gameForCards("54").isForNumberOfCards(32))
        Assert.assertFalse(gameForCards("54").isForNumberOfCards(52))
        Assert.assertTrue(gameForCards("54").isForNumberOfCards(54))
        Assert.assertFalse(gameForCards("54").isForNumberOfCards(64))
        Assert.assertFalse(gameForCards("54").isForNumberOfCards(104))
        Assert.assertTrue(gameForCards("54").isForNumberOfCards(108))
    }

    @Test
    fun isForNumberOfCards_64() {
        Assert.assertFalse(gameForCards("64").isForNumberOfCards(32))
        Assert.assertFalse(gameForCards("64").isForNumberOfCards(52))
        Assert.assertFalse(gameForCards("64").isForNumberOfCards(54))
        Assert.assertTrue(gameForCards("64").isForNumberOfCards(64))
        Assert.assertTrue(gameForCards("64").isForNumberOfCards(104))
        Assert.assertTrue(gameForCards("64").isForNumberOfCards(108))
    }

    @Test
    fun isForNumberOfCards_104() {
        Assert.assertFalse(gameForCards("104").isForNumberOfCards(32))
        Assert.assertFalse(gameForCards("104").isForNumberOfCards(52))
        Assert.assertFalse(gameForCards("104").isForNumberOfCards(54))
        Assert.assertFalse(gameForCards("104").isForNumberOfCards(64))
        Assert.assertTrue(gameForCards("104").isForNumberOfCards(104))
        Assert.assertTrue(gameForCards("104").isForNumberOfCards(108))
    }

    @Test
    fun isForNumberOfCards_108() {
        Assert.assertFalse(gameForCards("108").isForNumberOfCards(32))
        Assert.assertFalse(gameForCards("108").isForNumberOfCards(52))
        Assert.assertFalse(gameForCards("108").isForNumberOfCards(54))
        Assert.assertFalse(gameForCards("108").isForNumberOfCards(64))
        Assert.assertFalse(gameForCards("108").isForNumberOfCards(104))
        Assert.assertTrue(gameForCards("108").isForNumberOfCards(108))
    }

    @Test
    fun isForNumberOfCards_any() {
        Assert.assertTrue(gameForCards("?").isForNumberOfCards(32))
        Assert.assertTrue(gameForCards("?").isForNumberOfCards(52))
        Assert.assertTrue(gameForCards("?").isForNumberOfCards(54))
        Assert.assertTrue(gameForCards("?").isForNumberOfCards(64))
        Assert.assertTrue(gameForCards("?").isForNumberOfCards(104))
        Assert.assertTrue(gameForCards("?").isForNumberOfCards(108))
    }

    @Test
    fun isForNumberOfCards_complex() {
        Assert.assertTrue(gameForCards("102, 104, ?").isForNumberOfCards(32))
        Assert.assertTrue(gameForCards("102, 32, 104").isForNumberOfCards(32))
        Assert.assertTrue(gameForCards("102, 32, 104").isForNumberOfCards(64))
    }

    @Test
    fun isForNumberOfCards_other() {
        Assert.assertFalse(gameForCards("40").isForNumberOfCards(64))
        Assert.assertFalse(gameForCards("48").isForNumberOfCards(64))
    }

    private fun gameForPlayers(players: String): Game {
        return Game("name", null, null, "category", players,
            "32", null, null, "goal", "rules")
    }

    private fun gameForCards(cards: String): Game {
        return Game("name", null, null, "category", "2+",
            cards, null, null, "goal", "rules")
    }
}
