#+TITLE: Kartice
#+AUTHOR: Ondřej Kuhejda
/Kartice is Android application containing card games rules./
* Installation
  The application is uploaded on [[https://play.google.com/store/apps/details?id=me.kuhy.kartice][Google Play]]. Support for /F-Droid/ is planned in the future.
* Development
** Building
   App can be build using the following command:
   #+BEGIN_SRC sh
     ./gradlew assembleDebug
   #+END_SRC

   The resulting =APK= file can be then installed in the following way:
   #+BEGIN_SRC sh
     adb install -r -t app/build/outputs/apk/debug/app-debug.apk
   #+END_SRC

   Sometimes, it is also useful to clean the data of the application:
   #+BEGIN_SRC sh
     adb shell pm clear me.kuhy.kartice
   #+END_SRC
** Adding new rules
   The rules are stored in the =yaml= format. The text itself is in the
   CommonMark format with the GFM table support. Most of the rules were
   downloaded from the [[https://cs.wikibooks.org/wiki/Pokladnice_her/Klasick%C3%A9_karetn%C3%AD_hry][wikibooks]]. Wikibooks are using the MediaWiki markup which
   can be converted to the GFM using the following command:
   #+BEGIN_SRC sh
     pandoc --columns=80 -f mediawiki -t gfm
   #+END_SRC
** TODO Publishing
   1. Increment =versionCode= and =versionName= in [[file:app/build.gradle][build.gradle]].
   2. Increment =rules_version= in [[file:app/src/main/assets/app.properties][app.properties]]
      (only in case that game rules were changed).
   3. TODO
